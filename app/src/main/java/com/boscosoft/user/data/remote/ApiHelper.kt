package com.boscosoft.user.data.remote

import UsersResponse
import retrofit2.http.GET

interface ApiHelper {

    @GET("api/users")
    suspend fun getUsers(): UsersResponse

}