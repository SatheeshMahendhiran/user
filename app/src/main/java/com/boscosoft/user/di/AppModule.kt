package com.boscosoft.user.di

import com.boscosoft.user.data.remote.ApiHelper
import com.boscosoft.user.data.remote.RetrofitApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideApiHelper(retrofitApp: RetrofitApp): ApiHelper {
        return retrofitApp.apiHelper
    }

}