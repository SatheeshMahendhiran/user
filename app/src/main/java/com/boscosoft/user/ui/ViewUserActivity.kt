package com.boscosoft.user.ui

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.boscosoft.user.R
import com.boscosoft.user.databinding.ActivityViewUserBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class ViewUserActivity : AppCompatActivity(), OnMapReadyCallback {

    private val LOCATION_PERMESSION_REQUEST_CODE: Int = 101
    private val STORAGE_PERMESSION_REQUEST_CODE: Int = 102

    private val IMAGE_REQUEST_CODE: Int = 103
    private val VIDEO_REQUEST_CODE: Int = 104

    private lateinit var viewBinding: ActivityViewUserBinding

    private lateinit var context: Context

    private lateinit var gMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //View binding
        viewBinding = ActivityViewUserBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        context = this

        //Get values from intent
        val firstName: String? = intent.extras?.getString("firstName", "")
        val lastName: String? = intent.extras?.getString("lastName", "")
        val email: String? = intent.extras?.getString("email", "")
        val photoUrl: String? = intent.extras?.getString("avatar", "")
        val id: Int? = intent.extras?.getInt("id", 1)
        val name = "$firstName $lastName"

        viewBinding.textViewUserName.text = name
        viewBinding.textViewEmail.text = email
        viewBinding.textViewId.text = id.toString()

        val requestOptions: RequestOptions =
            RequestOptions().placeholder(R.drawable.ic_baseline_account_circle_24)
                .error(R.drawable.ic_baseline_account_circle_24)

        Glide.with(
            this
        ).load(photoUrl).apply(requestOptions).into(viewBinding.imageViewUser)

        viewBinding.mapView.getMapAsync(this)
        viewBinding.mapView.onCreate(savedInstanceState)
        viewBinding.progressBar.visibility = View.VISIBLE

        //On Click listeners
        viewBinding.textViewLocation.setOnClickListener {
            getLatLong()
        }

        viewBinding.imageView2.setOnClickListener {
            getLatLong()
        }

        viewBinding.imageViewAddImage.setOnClickListener {
            openGalleryIntent(IMAGE_REQUEST_CODE)
        }

        viewBinding.imageViewAddVideo.setOnClickListener {
            openGalleryIntent(VIDEO_REQUEST_CODE)
        }
    }

    private fun getLatLong() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Check the permission
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                //Request the permission
                requestPermissions(
                    arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION
                    ), LOCATION_PERMESSION_REQUEST_CODE
                )

                return
            }
        }

        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Please turn on GPS", Toast.LENGTH_SHORT).show()
            return
        }

        val location: Location? =
            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val lat = location?.latitude
        val long = location?.longitude

        val strLatLng = "$lat , $long"
        viewBinding.textViewLocation.text = strLatLng

        val latLong = LatLng(location?.latitude!!.toDouble(), location.longitude.toDouble())

        gMap?.addMarker(MarkerOptions().title("Current Position").position(latLong))
        gMap?.moveCamera(CameraUpdateFactory.newLatLng(latLong))

    }

    override fun onMapReady(map: GoogleMap) {
        gMap = map
        viewBinding.progressBar.visibility = View.GONE
        getLatLong()
    }

    private fun openGalleryIntent(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Check the permission
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                //Request the permission
                requestPermissions(
                    arrayOf(
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    ), STORAGE_PERMESSION_REQUEST_CODE
                )

                return
            }
        }

        //launchGalleryIntent();
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(
            gallery,
            requestCode
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMESSION_REQUEST_CODE) {
            if (grantResults.size > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLatLong()
                } else {
                    Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == STORAGE_PERMESSION_REQUEST_CODE) {
            if (grantResults.size > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        this,
                        "Storage Permission granted, You can access the files",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(this, "Storage Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST_CODE) {
            val uri: Uri? = data?.data

            if (uri == null) {
                Toast.makeText(
                    this,
                    "Couldn't pick the image, Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }

            val filePath: String = getPathFromURI(this, uri)
            if (filePath == null || filePath.isEmpty()) {
                Toast.makeText(
                    this,
                    "Couldn't pick the image, Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }

            val fileFormat = filePath.subSequence(filePath.lastIndexOf("."), filePath.length)
            if (fileFormat.equals(".jpg") || fileFormat.equals(".png")) {
                viewBinding.imageViewUser.setImageURI(uri)
            } else {
                Toast.makeText(
                    this,
                    "Please select .png or .jpg files",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else if (requestCode == VIDEO_REQUEST_CODE) {
            val uri: Uri? = data?.data

            if (uri == null) {
                Toast.makeText(
                    this,
                    "Couldn't pick the video, Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }

            val filePath: String = getPathFromURI(this, uri)
            if (filePath == null || filePath.isEmpty()) {
                Toast.makeText(
                    this,
                    "Couldn't pick the video, Please try again",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }

            val fileFormat = filePath.subSequence(filePath.lastIndexOf("."), filePath.length)
            if (fileFormat.equals(".mp4") || fileFormat.equals(".3gp")) {
                val intent = Intent(
                    this,
                    ViewFileActivity::class.java
                )
                intent.putExtra("path", uri.toString())
                startActivity(intent)
            } else {
                Toast.makeText(
                    this,
                    "Please select .mp4 or .3gp files",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun getPathFromURI(context: Context, uri: Uri): String {
        val queryUri = MediaStore.Files.getContentUri("external")
        val columnData = MediaStore.Files.FileColumns.DATA
        val columnSize = MediaStore.Files.FileColumns.SIZE

        val projectionData = arrayOf(MediaStore.Files.FileColumns.DATA)

        var name: String? = null
        var size: String? = null

        val cursor = context.contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.count > 0) {
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
            cursor.moveToFirst()
            name = cursor.getString(nameIndex)
            size = cursor.getString(sizeIndex)
            cursor.close()
        }

        var imagePath = ""
        if (name != null && size != null) {
            val selectionNS = "$columnData LIKE '%$name' AND $columnSize='$size'"
            val cursorLike =
                context.contentResolver.query(queryUri, projectionData, selectionNS, null, null)
            if (cursorLike != null && cursorLike.count > 0) {
                cursorLike.moveToFirst()
                val indexData = cursorLike.getColumnIndex(columnData)
                if (cursorLike.getString(indexData) != null) {
                    imagePath = cursorLike.getString(indexData)
                }
                cursorLike.close()
            }
        }

        return imagePath!!
    }

    /*
    * Lifecycle methods
    * */
    override fun onStart() {
        super.onStart()
        viewBinding.mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        viewBinding.mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewBinding.mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        viewBinding.mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewBinding.mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        viewBinding.mapView.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        viewBinding.mapView.onSaveInstanceState(outState)
    }

}