package com.boscosoft.user.ui.users

import User
import UsersResponse
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.boscosoft.user.R
import com.boscosoft.user.databinding.ActivityUsersBinding
import com.boscosoft.user.model.ApiResponse
import com.boscosoft.user.ui.ViewUserActivity
import com.google.android.material.animation.AnimationUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UsersActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityUsersBinding
    private val usersViewModel: UsersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Init View Binding
        viewBinding = ActivityUsersBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        //Start process
        setUpUsersObserver()
    }

    //Setup observer to get user details from view model
    private fun setUpUsersObserver() {
        usersViewModel.getUsers()
        usersViewModel.usersResponseLiveData.observe(this, Observer {
            when (it) {
                is ApiResponse.Loading -> {
                    viewBinding.progressBar.visibility = View.VISIBLE
                }
                is ApiResponse.Success -> {
                    viewBinding.progressBar.visibility = View.GONE
                    val usersResponse: UsersResponse = it.value as UsersResponse

                    val listUser: List<User> = usersResponse.data
                    showUsers(listUser)
                }
                is ApiResponse.Failure -> {
                    viewBinding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    //Show users in recyclerview
    private fun showUsers(listUser: List<User>) {
        val mUserAdapter =
            UsersAdapter(this, listUser, { item -> onRecyclerItemClick(item) })
        viewBinding.recyclerViewUser.layoutManager = LinearLayoutManager(this)
        viewBinding.recyclerViewUser.layoutAnimation =
            android.view.animation.AnimationUtils.loadLayoutAnimation(
                this,
                R.anim.item_layout_left_to_right_anim
            )
        viewBinding.recyclerViewUser.adapter = mUserAdapter
    }

    private fun onRecyclerItemClick(user: User) {
        val intent = Intent(
            this,
            ViewUserActivity::class.java
        )
        intent.putExtra("firstName", user.first_name)
        intent.putExtra("lastName", user.last_name)
        intent.putExtra("avatar", user.avatar)
        intent.putExtra("email", user.email)
        intent.putExtra("id", user.id)
        startActivity(intent)
    }
}