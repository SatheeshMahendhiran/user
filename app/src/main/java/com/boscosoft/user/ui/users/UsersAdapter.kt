package com.boscosoft.user.ui.users

import User
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.boscosoft.user.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

class UsersAdapter(
    private val context: Context,
    private val mUserList: List<User>,
    val onRecyclerItemClick: (User) -> Unit
) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    private var requestOptions: RequestOptions =
        RequestOptions().placeholder(R.drawable.ic_baseline_account_circle_24)
            .error(R.drawable.ic_baseline_account_circle_24)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName: TextView
        val textViewEmail: TextView
        val imageView: CircleImageView

        init {
            textViewName = itemView.findViewById(R.id.textViewUserName)
            textViewEmail = itemView.findViewById(R.id.textViewEmail)
            imageView = itemView.findViewById(R.id.imageViewUser)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user: User = mUserList.get(position)

        val name = user.first_name + " " + user.last_name
        holder.textViewName.text = name
        holder.textViewEmail.text = user.email

        Glide.with(context).load(user.avatar).apply(requestOptions).into(holder.imageView)

        holder.itemView.setOnClickListener(View.OnClickListener {
            onRecyclerItemClick(user)
        })
    }

    override fun getItemCount(): Int {
        return mUserList.size
    }

}