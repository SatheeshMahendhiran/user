package com.boscosoft.user.ui.users

import com.boscosoft.user.data.remote.ApiHelper
import com.boscosoft.user.model.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import javax.inject.Inject

class UserRepository @Inject constructor(val apiHelper: ApiHelper) {

    suspend fun getUsers(): Flow<ApiResponse<Any>> = flow {
        emit(ApiResponse.Loading)
        try {
            val result = apiHelper.getUsers()
            emit(ApiResponse.Success(result))
        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    ApiResponse.Failure(
                        false,
                        throwable.code(),
                        throwable.response()?.errorBody()
                    )
                }
                else -> {
                    ApiResponse.Failure(true, null, null)
                }
            }
        }
    }
}