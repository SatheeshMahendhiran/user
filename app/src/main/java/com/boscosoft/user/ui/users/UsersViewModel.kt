package com.boscosoft.user.ui.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.boscosoft.user.model.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(val userRepository: UserRepository) : ViewModel() {

    /*
    * MutableLive data & getter for live data
    * */
    private val mutableLiveUsersData: MutableLiveData<ApiResponse<Any>> =
        MutableLiveData()
    val usersResponseLiveData: LiveData<ApiResponse<Any>> get() = mutableLiveUsersData

    fun getUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.getUsers().collect {
                mutableLiveUsersData.postValue(it)
            }
        }
    }

}