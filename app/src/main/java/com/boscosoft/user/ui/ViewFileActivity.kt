package com.boscosoft.user.ui

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import com.boscosoft.user.databinding.ActivityViewFileBinding

class ViewFileActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityViewFileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityViewFileBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        val path: String? = intent.extras?.getString("path", "")

        viewBinding.videoView.setVideoURI(Uri.parse(path))

        val mediaController = MediaController(this)
        viewBinding.videoView.setMediaController(mediaController)
        mediaController.setMediaPlayer(viewBinding.videoView)
        viewBinding.videoView.start()

    }
}